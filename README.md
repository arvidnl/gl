# `gl` - GitLab REST API Wrench

`gl` is a tool for querying GitLab's REST API.

## Requirements

`jq`, `curl`, `git`.

Optional requirements: [ijq](https://sr.ht/~gpanders/ijq/) and `less`.

## Installation

Clone this repository and put `gl` somewhere in path.

## Usage

To query a GitLab endpoint, e.g. the list of [merge
requests](https://docs.gitlab.com/ee/api/merge_requests.html#list-merge-requests)
in a project, pass the endpoint as the first unnamed argument of
`gl`. For instance, to retrieve the list of all merge requests:

```
gl --project tezos/tezos 'merge_requests'
```

The second, unnamed argument is an optional
[jq](https://jqlang.github.io/jq/) expression that is applied to the
result of the API call. For instance

```
gl --project tezos/tezos 'merge_requests' 'map(select(.draft))|.id'
```

will retrieve the id of the merge requests that are in draft.

See `gl --help` for more information.

# Autocompletion

`gl` includes a zsh plugin that enables auto-completion for API
endpoints. To use it, source `zsh-plugin/_gl` after modifying the path
contained therein to point to where you've cloned this repository.
